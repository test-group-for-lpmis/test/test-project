# test-project

This is a simple project to demonstrate how external contribution can be facilitated.

## Instalation

1. Clone the repo:
   git clone https://code.europa.eu/test-group-for-lpmis/test/test-project

2. Navigate to the project directory:
   cd test-project

3. Install dependencies:
   npm instal

## Usage

To run the project, use the following command:
   npm start

## Contributing

1. Fork the repo
2. Create a new branch (git checkout -b feature/AmazingFeature)
3. Commit your changes (git commit -m 'Add some AmazingFeature')
4. Push to the branch (git push origin feature/AmazingFeature)
5. Open a Pull Request
